# @universis/discord-connect

This module allows Universis to connect with Discord.

In this module, a Discord bot connection is made and is used to notify users on Discord when they receive a message by Universis.

> __Note__:
> Newer versions of `discord.js` do not support Node.js 14 due to breaking changes, therefore this module uses `discord.js@12`.
> Due to this factor, we might be limited regarding the features we can implement.

## ToDo

- [ ] **Review** - Discord Authorization scopes.

## Installation

In your `universis-api` project, run:

```bash
$ cd modules
$ npm install @universis/discord-connect
$ cd discord-connect
$ npm ci
```

## Discord Application Setup

- Create a new application in [Discord Developer Portal](https://discord.com/developers/applications/).
- Switch to the `Bot` tab and create a new bot.
- Click `Reset Token` and copy the token. You're going to need it when configuring the module.
- Switch to the `OAuth2` tab, then select `URL Generator`. Select `bot` in Scopes and `Administrator` in Bot Permissions.
  Copy the URL and open it in your browser to invite the bot to your server.
  This URL can be used to invite the bot to multiple servers.
- Switch to the `General` tab and add `http://localhost:5001/services/discord/redirect` in Redirects. Again, select `bot` in Scopes and `Administrator` in Bot Permissions.

## Configuration

**A Universis client with `profile` scope is required for this module to work.**

Do the following edits in your `universis-api` project

- Add the following under `settings/universis` in  `server/config/app.development.json`
  ```json
  ...
  "universis": {
    ...
    "discord-connect": {
      "token": "<Your_Discord_Bot_Token>",
      "client_id": "<Your_Discord_Application_ID>",
      "client_secret": "<Your_Discord_OAuth2_Client_Secret>",
      "redirect_uri": "http://localhost:5001/services/discord/redirect",
      "web": "http://localhost:5001/services/discord/login",
      "prefix": "!",
      "auth": {
        "authorizationURL": "https://users.universis.io/authorize",
        "logoutURL": "https://users.universis.io/logout?continue=http://localhost:5001/services/discord/logout",
        "tokenURL": "https://users.universis.io/access_token",
        "scope": "profile",
        "clientID": "<Your_Universis_Client_ID>",
        "clientSecret": "<Your_Universis_Client_Secret>",
        "callbackURL": "http://localhost:5001/services/discord/callback"
      }
    }
  },
  ...
  ```

- Add the following in `.module-aliases.json`
  ```json
  ...
  "@universis/discord-connect": "modules/discord-connect/src/index"
  ...
  ```
