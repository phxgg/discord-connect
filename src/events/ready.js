/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
import { ExpressDataApplication } from "@themost/express";
import { Client } from "discord.js";

export default {
  name: 'ready',
  once: true,
  /**
   * 
   * @param {Client} client 
   * @param {ExpressDataApplication} app
   */
  execute(client, app) {
    console.log(`[discord-connect] Logged in as ${client.user.tag}!`);
  }
}