/* eslint-disable no-unused-vars */
/* eslint-disable no-console */
import { ExpressDataApplication } from "@themost/express";
import { Client, Message } from "discord.js";

// Discord.js 12 hardly supports slash commands, so we have to use the old way of listening to messages to create commands
export default {
  name: 'message',
  once: false,
  /**
   * 
   * @param {Message} message 
   * @param {Client} client 
   * @param {ExpressDataApplication} app
   * @returns 
   */
  execute(message, client, app) {
    if (message.author.bot) return;

    if (!message.content.startsWith(client.prefix)) return;

    // Split message into command and arguments
    const args = message.content.slice(client.prefix.length).trim().split(/ +/);
    const command = args.shift().toLowerCase();

    if (!client.commands.has(command)) return;

    try {
      client.commands.get(command).execute(message, app, args);
    } catch (err) {
      console.error(err);
      message.reply('There was an error trying to execute that command!');
    }
  }
}