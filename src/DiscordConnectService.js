/* eslint-disable no-unused-vars */
/* eslint-disable no-console */

import { ApplicationService, TraceUtils } from "@themost/common";
import LocalScopeAccessConfiguration from './config/scope.access.json';
import { AccountReplacer } from './AccountReplacer';
import { InstantMessageReplacer } from './InstantMessageReplacer';
import path from 'path';
import fs from 'fs';
import { discordConnectRouter } from './routers/discordConnectRouter';

/**
 * @param {Router} parent
 * @param {Router} before
 * @param {Router} insert
 */
function insertRouterBefore(parent, before, insert) {
  const beforeIndex = parent.stack.findIndex((item) => {
    return item === before;
  });
  if (beforeIndex < 0) {
    throw new Error('Target router cannot be found in parent stack.');
  }
  const findIndex = parent.stack.findIndex((item) => {
    return item === insert;
  });
  if (findIndex < 0) {
    throw new Error('Router to be inserted cannot be found in parent stack.');
  }
  // remove last router
  parent.stack.splice(findIndex, 1);
  // move up
  parent.stack.splice(beforeIndex, 0, insert);
}

export class DiscordConnectService extends ApplicationService {
  constructor(app) {

    new AccountReplacer(app).apply();
    new InstantMessageReplacer(app).apply();

    super(app);

    // Try to initialize discord client and connect to our bot
    try {
      const Discord = require('discord.js');

      // Some intents do not exist in the discord.js 12, so we have to use the Bitfield method
      // Discord intents calculator: https://discord-intents-calculator.vercel.app/
      const myIntents = new Discord.Intents(37379);
      this.client = new Discord.Client({ ws: { intents: [myIntents] } });

      const discordConfig = app.getConfiguration().getSourceAt('settings/universis/discord-connect');
      this.client.prefix = discordConfig.prefix || '!';

      // Load commands
      this.client.commands = new Discord.Collection();
      const commandFiles = fs.readdirSync(path.resolve(__dirname, 'commands')).filter(file => file.endsWith('.js'));
      for (const file of commandFiles) {
        const command = require(path.resolve(__dirname, 'commands', file));
        this.client.commands.set(command.default.name, command.default);
      }

      // Handle events
      const eventFiles = fs.readdirSync(path.resolve(__dirname, 'events')).filter(file => file.endsWith('.js'));
      for (const file of eventFiles) {
        const event = require(path.resolve(__dirname, 'events', file));
        if (event.once)
          this.client.once(event.default.name, (...args) => event.default.execute(...args, this.client, app));
        else
          this.client.on(event.default.name, (...args) => event.default.execute(...args, this.client, app));
      }

      // Discord application login
      setTimeout(() => {
        this.client.login(discordConfig.token);
      }, 1000);
    } catch (err) {
      console.log('[discord-connect] Something went wrong while trying to connect to discord. Printing error...');
      console.error(err);
    }

    // extend universis api scope access configuration
    if (app && app.container) {
      app.container.subscribe((container) => {
        if (container) {
          // use router
          container.use('/services/discord', discordConnectRouter(app)); // todo: rename to /services/discord
          // get container router
          const router = container._router;
          // find before position
          const before = router.stack.find((item) => {
            return item.name === 'dataContextMiddleware';
          });
          if (before == null) {
            // do nothing
            return;
          }
          const insert = router.stack[router.stack.length - 1];
          // re-index router
          insertRouterBefore(router, before, insert);

          // add extra scope access elements
          const scopeAccess = app.getConfiguration().getStrategy(function ScopeAccessConfiguration() { });
          if (scopeAccess != null) {
            scopeAccess.elements.push.apply(scopeAccess.elements, LocalScopeAccessConfiguration);
          }
        }
      });
    } else {
      // container is missing
      TraceUtils.warn('DiscordConnectService', 'Application container is missing. Discord connect service endpoint will be unavailable.');
    }
  }

  sendDirectMessage(member, body) {
    if (!member.discordAccount || !member.discordAccount.identifier) {
      console.log(`[discord-connect] Member ${member.name} has no discord account`);
      return;
    }

    this.client.users.fetch(member.discordAccount.identifier).then(user => {
      user.send(body);
    }).catch(err => {
      console.log(err);
    });
  }

}