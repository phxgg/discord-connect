/* eslint-disable no-unused-vars */
import { ExpressDataApplication } from "@themost/express";
import { Message } from "discord.js";

export default {
  name: 'ping',
  description: 'Ping!',
  /**
   * 
   * @param {Message} message
   * @param {ExpressDataApplication} app
   * @param {*} args
   */
  execute(message, app, args) {
    message.channel.send('Pong.');
  }
}