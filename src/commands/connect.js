/* eslint-disable no-console */
/* eslint-disable no-unused-vars */
import { ExpressDataApplication } from "@themost/express";
import { Message, MessageEmbed } from "discord.js";

export default {
  name: 'connect',
  description: 'Connect your Discord account to your Universis account.',
  /**
   * 
   * @param {Message} message
   * @param {ExpressDataApplication} app
   * @param {*} args
   * @returns
   */
  async execute(message, app, args) {
    const discordConfig = app.getConfiguration().getSourceAt('settings/universis/discord-connect');
    
    const context = app.createContext();

    try {
      const user = await context.model('User').where('discordAccount/identifier').equal(message.author.id).silent().getItem();

      const embed = new MessageEmbed();

      // colors:
      // #0099ff - blue
      // #ff6a6a - red
      // #89ff89 - green

      if (user) {
        embed.setTitle('Account already connected');
        embed.setDescription('Your Discord account is already connected to your Universis account.');
        embed.addField('Modify connection', discordConfig.web);
        embed.setColor('#89ff89');
      } else {
        embed.setTitle('Connect your account');
        embed.setDescription('Click on the link below to connect your account to Universis.');
        embed.addField('Login link', discordConfig.web);
        embed.setColor('#0099ff');
      }

      // Send the embed to author as a DM
      message.author.send(embed);
    } catch (err) {
      console.error(err);
      message.author.send('An error occurred. Please try again later.');
    }

    // finalize context
    await context.finalizeAsync();
  }
}